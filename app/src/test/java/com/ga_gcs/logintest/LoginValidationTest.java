package com.ga_gcs.logintest;

import com.ga_gcs.logintest.helpers.GAHelper;

import org.junit.Assert;
import org.junit.Test;

public class LoginValidationTest {

    @Test
    public void test_email_without_domain() {
        Assert.assertEquals(false, GAHelper.validateEmail("email"));
    }

    @Test
    public void test_email_invalid_domain() {
        Assert.assertEquals(false, GAHelper.validateEmail("email.com"));
    }

    @Test
    public void test_email_without_dot_com() {
        Assert.assertEquals(false, GAHelper.validateEmail("email@gmail"));
    }

    @Test
    public void test_email_valid() {
        Assert.assertEquals(true, GAHelper.validateEmail("email@gmail.com"));
    }

    @Test
    public void test_email_empty() {
        Assert.assertEquals(false, GAHelper.validateEmail(""));
    }

    @Test
    public void test_password_length_invalid() {
        Assert.assertEquals(false, GAHelper.validatePassword("pass"));
    }

    @Test
    public void test_password_empty() {
        Assert.assertEquals(false, GAHelper.validatePassword(""));
    }

    @Test
    public void test_password_invalid_length() {
        Assert.assertEquals(false, GAHelper.validatePassword("passw"));
    }

    @Test
    public void test_password_no_special_character() {
        Assert.assertEquals(false, GAHelper.validatePassword("Password1"));
    }

    @Test
    public void test_password_no_capital_letter() {
        Assert.assertEquals(false, GAHelper.validatePassword("@password1"));
    }

    @Test
    public void test_password_no_number() {
        Assert.assertEquals(false, GAHelper.validatePassword("@Passwordpassword"));
    }

    @Test
    public void test_password_valid() {
        Assert.assertEquals(true, GAHelper.validatePassword("@Password123"));
    }


//    @ParameterizedTest(name = "#{index} - Run test with password = {0}")
//    @MethodSource("validPasswordProvider")
//    public void test_password_regex_valid(String password) {
//        assertTrue(GAHelper.validatePassword(password));
//    }
//
//    @ParameterizedTest(name = "#{index} - Run test with password = {0}")
//    @MethodSource("invalidPasswordProvider")
//    public void test_password_regex_invalid(String password) {
//        assertFalse(GAHelper.validatePassword(password));
//    }
//
//    static Stream<String> validPasswordProvider() {
//        return Stream.of(
//                "AAAbbbccc@123",
//                "Hello world$123",
//                "A!@#&()–a1",               // test punctuation part 1
//                "A[{}]:;',?/*a1",           // test punctuation part 2
//                "A~$^+=<>a1",               // test symbols
//                "0123456789$abcdefgAB",     // test 20 chars
//                "123Aa$Aa"                  // test 8 chars
//        );
//    }
//
//    // At least
//    // one lowercase character,
//    // one uppercase character,
//    // one digit,
//    // one special character
//    // and length between 8 to 20.
//    static Stream<String> invalidPasswordProvider() {
//        return Stream.of(
//                "12345678",                 // invalid, only digit
//                "abcdefgh",                 // invalid, only lowercase
//                "ABCDEFGH",                 // invalid, only uppercase
//                "abc123$$$",                // invalid, at least one uppercase
//                "ABC123$$$",                // invalid, at least one lowercase
//                "ABC$$$$$$",                // invalid, at least one digit
//                "java REGEX 123",           // invalid, at least one special character
//                "java REGEX 123 %",         // invalid, % is not in the special character group []
//                "________",                 // invalid
//                "--------",                 // invalid
//                " ",                        // empty
//                "");                        // empty
//    }
}
