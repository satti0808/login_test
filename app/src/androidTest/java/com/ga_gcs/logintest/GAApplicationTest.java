package com.ga_gcs.logintest;

import com.ga_gcs.logintest.application.GAApplication;

public class GAApplicationTest extends GAApplication {

    @Override
    public String getBaseUrl() {
        return "http://127.0.0.1:8080";
    }
}
