package com.ga_gcs.logintest;

import static androidx.test.InstrumentationRegistry.getContext;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static org.awaitility.Awaitility.await;
import static java.util.concurrent.TimeUnit.SECONDS;

import android.app.Activity;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.ga_gcs.logintest.application.GAApplication;
import com.ga_gcs.logintest.helpers.GAConstants;
import com.ga_gcs.logintest.ui.Login.LoginActivity;
import com.ga_gcs.logintest.ui.Login.LoginRepository;
import com.ga_gcs.logintest.ui.Login.LoginViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;

import kotlin.jvm.JvmField;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Rule
    public ActivityScenarioRule<LoginActivity> scenarioRule = new ActivityScenarioRule<LoginActivity>(LoginActivity.class);

//    private MockWebServer mockWebServer = new MockWebServer();
//
//    private final Dispatcher successDispatcher = new Dispatcher() {
//        @Override
//        public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
//            if (request.getPath().equals("/User/v1/login")) {
//                String fileName = "success_response.json";
//                try {
//                    return new MockResponse().setResponseCode(200).setBody(RestServiceTestHelper.getStringFromFile(getContext(), fileName));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            return new MockResponse().setResponseCode(404);
//        }
//    };

//    @Before
//    public void setup() throws IOException {
//        mockWebServer.start(8080);
//        mockWebServer.setDispatcher(successDispatcher);
//    }

    @Test
    public void test_display_incorrect_email() {
        String incorrectEmail = "email";
        onView(withId(R.id.edt_login_id)).perform(typeText(incorrectEmail));
        onView(withId(R.id.btn_sign_in)).perform(click());
        onView(withId(com.google.android.material.R.id.snackbar_text)).check(matches(withText(R.string.login_failed)));
    }

    @Test
    public void test_display_incorrect_password() {
        String incorrectEmail = "password";
        onView(withId(R.id.edt_password)).perform(typeText(incorrectEmail));
        onView(withId(R.id.btn_sign_in)).perform(click());
        onView(withId(com.google.android.material.R.id.snackbar_text)).check(matches(withText(R.string.login_failed)));
    }

//    @Test
//    public void test_success_response() throws Exception {
//
////        scenarioRule.getScenario().onActivity(new ActivityScenario.ActivityAction<LoginActivity>() {
////            @Override
////            public void perform(LoginActivity activity) {
////                loginViewModel = activity.getLoginViewModel();
////            }
////        });
//
////        String fileName = "success_response.json";
////        MockResponse mockResponse = new MockResponse()
////                .setResponseCode(200)
////                .setBody(RestServiceTestHelper.getStringFromFile(getContext(), fileName));
////        mockWebServer.enqueue(mockResponse);
//
//        String email = "rahul.suresh@generalaeronautics.com";
//        String password = "Demo@123456789";
//        onView(withId(R.id.edt_login_id)).perform(typeText(email));
//        onView(withId(R.id.edt_password)).perform(typeText(password));
//        onView(withId(R.id.btn_sign_in)).perform(click());
//
////        await().atMost(20, SECONDS).until( () -> loginViewModel.getLoginResponse().getValue() != null);
//        onView(withId(com.google.android.material.R.id.snackbar_text)).check(matches(withText("Welcome Rahul S")));
//    }

//    @After
//    public void tearDown() throws IOException {
//        mockWebServer.shutdown();
//    }
}
