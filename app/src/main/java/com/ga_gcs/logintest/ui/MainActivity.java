package com.ga_gcs.logintest.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ga_gcs.logintest.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}