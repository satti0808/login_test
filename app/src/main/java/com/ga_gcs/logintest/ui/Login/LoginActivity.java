package com.ga_gcs.logintest.ui.Login;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.ga_gcs.logintest.R;
import com.ga_gcs.logintest.application.GAApplication;
import com.ga_gcs.logintest.databinding.ActivityLoginBinding;
import com.ga_gcs.logintest.helpers.GAHelper;
import com.ga_gcs.logintest.models.Login.User;
import com.ga_gcs.logintest.models.Login.UserData;
import com.ga_gcs.logintest.models.Requests.LoginRequest;
import com.ga_gcs.logintest.models.Responses.LoginResponse;
import com.ga_gcs.logintest.models.Responses.ServerResponse;
import com.ga_gcs.logintest.ui.GAActivity;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.TestOnly;

public class LoginActivity extends GAActivity implements View.OnClickListener {

    ActivityLoginBinding binding;
    LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);

//        /*  TODO Hardcoded for testing  */
//        binding.tilLoginId.getEditText().setText("rahul.suresh@generalaeronautics.com");
//        binding.tilPassword.getEditText().setText("Demo@1234");

        binding.btnSignIn.setOnClickListener(this);
    }

    private void initObserver() {
        loginViewModel.getLoginResponse().observe(
                LoginActivity.this, new Observer<LoginResponse>() {
                    @Override
                    public void onChanged(LoginResponse loginResponse) {
                        String message = "";
                        if (loginResponse.getHttpCode() == 200 && loginResponse.getUser() != null) {
                            UserData userData = loginResponse.getUser().getUserData();
                            message = "Welcome " + userData.getFullName();
                        } else {
                            if (loginResponse.getHttpCode() == 400) {
                                message = loginResponse.getServerResponse().getMessage();
                            } else {
                                message = loginResponse.getError().getError_description();
                            }
                        }
                        Snackbar.make(binding.getRoot(), message, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        if(v == binding.btnSignIn) {
            closeKeyboard();
            String userName = GAHelper.getTILText(binding.tilLoginId);
            String password = GAHelper.getTILText(binding.tilPassword);
            if (GAHelper.validateEmail(userName)
                    && GAHelper.validatePassword(password)) {
                if (GAApplication.getInstance().isNetworkAvailable()) {
                    LoginRequest loginRequest = new LoginRequest(userName, password);
                    loginViewModel.getLoginUserDetails(loginRequest);
                }
            } else {
                Snackbar.make(binding.getRoot(), getString(R.string.login_failed),
                        Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        initObserver();
    }

//    @TestOnly
//    public LoginViewModel getLoginViewModel() {
//        return loginViewModel;
//    }
}
