package com.ga_gcs.logintest.ui.Login;

import android.text.TextUtils;

import androidx.lifecycle.MutableLiveData;

import com.ga_gcs.logintest.application.GAApplication;
import com.ga_gcs.logintest.models.Login.User;
import com.ga_gcs.logintest.models.Requests.LoginRequest;
import com.ga_gcs.logintest.models.Responses.Error;
import com.ga_gcs.logintest.models.Responses.LoginResponse;
import com.ga_gcs.logintest.models.Responses.ServerResponse;
import com.ga_gcs.logintest.services.GAApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository {

    private final String TAG = getClass().getSimpleName();

    public MutableLiveData<LoginResponse> requestLoginDetails(LoginRequest loginRequest) {
        LoginResponse loginResponse = new LoginResponse();
        MutableLiveData<LoginResponse> mutableLiveData = new MutableLiveData<>();
        GAApiInterface apiInterface = GAApplication.getInstance().getRetrofitClient().create(GAApiInterface.class);
        apiInterface.getLoginUser(loginRequest).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                loginResponse.setHttpCode(response.code());
                if (response.isSuccessful() && response.body() != null) {
                    JsonObject jsonObject = response.body().getAsJsonObject();
                    String strUser = jsonObject.toString();
                    User user = new Gson().fromJson(strUser, User.class);
                    loginResponse.setUser(user);
                } else {
                    int statusCode = response.code();
                    ResponseBody responseBody = response.errorBody();
                    String strUser = null;
                    try {
                        if (responseBody != null) {
                            strUser = responseBody.string();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (statusCode == 401) {
                        if (!TextUtils.isEmpty(strUser)) {
                            Error error = new Gson().fromJson(strUser, Error.class);
                            loginResponse.setError(error);
                        }
                    } else if (statusCode == 400) {
                        if (!TextUtils.isEmpty(strUser)) {
                            ServerResponse serverResponse = new Gson().fromJson(strUser, ServerResponse.class);
                            loginResponse.setServerResponse(serverResponse);
                        }
                    }
                }
                mutableLiveData.postValue(loginResponse);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return mutableLiveData;
    }
}
