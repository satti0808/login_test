package com.ga_gcs.logintest.ui.Login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ga_gcs.logintest.models.Requests.LoginRequest;
import com.ga_gcs.logintest.models.Responses.LoginResponse;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginResponse> mutableLiveData = new MutableLiveData<>();
    private LoginRepository loginRepository;

    public LoginViewModel() {
        loginRepository = new LoginRepository();
    }

    public void getLoginUserDetails(LoginRequest loginRequest) {
        mutableLiveData.setValue(loginRepository.requestLoginDetails(loginRequest).getValue());
    }

    public LiveData<LoginResponse> getLoginResponse() {
        return mutableLiveData;
    }
}
