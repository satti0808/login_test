package com.ga_gcs.logintest.models.Login;

public class UserData {
    private long id;
    private String full_name;
    private String email;
    private String mobile;
    private String designation;
    private String photo;
    private String address;
    private String status;
    private Role role;
    private Customer customer;

    public long getID() {
        return id;
    }

    public void setID(long value) {
        this.id = value;
    }

    public String getFullName() {
        return full_name;
    }

    public void setFullName(String value) {
        this.full_name = value;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String value) {
        this.mobile = value;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String value) {
        this.designation = value;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String value) {
        this.photo = value;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String value) {
        this.address = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String value) {
        this.status = value;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role value) {
        this.role = value;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer value) {
        this.customer = value;
    }
}
