package com.ga_gcs.logintest.models.Responses;

public class ServerResponse {
    private long statusCode;
    private String error;
    private String message;
    private Validation validation;

    public long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(long value) {
        this.statusCode = value;
    }

    public String getError() {
        return error;
    }

    public void setError(String value) {
        this.error = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String value) {
        this.message = value;
    }

    public Validation getValidation() {
        return validation;
    }

    public void setValidation(Validation value) {
        this.validation = value;
    }
}
