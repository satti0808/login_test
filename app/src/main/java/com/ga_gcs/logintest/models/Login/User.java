package com.ga_gcs.logintest.models.Login;

public class User {
    private AuthData auth_data;
    private UserData user_data;

    public AuthData getAuthData() {
        return auth_data;
    }

    public void setAuthData(AuthData value) {
        this.auth_data = value;
    }

    public UserData getUserData() {
        return user_data;
    }

    public void setUserData(UserData value) {
        this.user_data = value;
    }
}
