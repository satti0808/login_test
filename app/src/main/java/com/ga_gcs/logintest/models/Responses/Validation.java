package com.ga_gcs.logintest.models.Responses;

import java.util.List;

public class Validation {
    private String source;
    private List<String> keys;

    public String getSource() { return source; }
    public void setSource(String value) { this.source = value; }

    public List<String> getKeys() { return keys; }
    public void setKeys(List<String> value) { this.keys = value; }
}
