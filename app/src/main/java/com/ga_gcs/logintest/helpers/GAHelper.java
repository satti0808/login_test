package com.ga_gcs.logintest.helpers;

import android.text.TextUtils;

import androidx.core.util.PatternsCompat;

import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GAHelper {

    public static String getTILText(TextInputLayout textInputLayout) {
        return textInputLayout.getEditText().getText().toString().trim();
    }

    public static boolean validateEmail(String email) {
        return !TextUtils.isEmpty(email) && PatternsCompat.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean validatePassword(String password) {
        return !TextUtils.isEmpty(password) &&
                Pattern.compile(GAConstants.PATTERN_PASSWORD).matcher(password).matches();
    }

}
